package io.renren;

import cn.hutool.core.io.FileUtil;
import org.flowable.common.engine.impl.util.IoUtil;
import org.flowable.engine.*;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskLogEntry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FlowTest {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private IdentityService identityService;

    @Test
    public void init() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println(processEngine);
    }

    @Test
    public void createDeploymentAddClasspathResource() {
        String resource = "processes/QualityManagementProcess.bpmn20.xml";
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource(resource)
                //修改流程部署分类
                //对应ACT_RE_DEPLOYMENT的CATEGORY_字段
                .category("flow_1")
                .deploy();
        System.out.println(deployment);
    }

    @Test
    public void createDeploymentAddBytes() {
        String resourceName = "processes/QualityManagementProcess.bpmn20.xml";
        InputStream resourceAsStream = FlowTest.class.getClassLoader().getResourceAsStream("processes/QualityManagementProcess.bpmn20.xml");
        byte[] resourceBytes = IoUtil.readInputStream(resourceAsStream, "resourceAsStream");
        Deployment deployment = repositoryService.createDeployment()
                .addBytes(resourceName, resourceBytes)
                .category("flow_1")
                .deploy();
        System.out.println(deployment);
    }

    /**
     * 修改流程定义分类
     * 对应ACT_RE_PROCDEF的CATEGORY_字段
     */
    @Test
    public void setProcessDefinitionCategory() {
        String processDefinitionId = "QualityManagementProcess:1:292064c9-0c05-11ea-9a48-e0d55eaf548e";
        String category = "flow_2";
        repositoryService.setProcessDefinitionCategory(processDefinitionId, category);
    }

    @Test
    public void deleteDeployment() {
        String deploymentId = "";
        repositoryService.deleteDeployment(deploymentId);
    }

    @Test
    public void createDeploymentQuery() {
        List<Deployment> deployments = repositoryService.createDeploymentQuery().list();
        for (Deployment deployment : deployments) {
            System.out.println(deployment.getName());
            System.out.println(deployment.getCategory());
        }
    }

    @Test
    public void createProcessDefinitionQuery() {
        List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().list();
        for (ProcessDefinition processDefinition : processDefinitions) {
            System.out.println(processDefinition.getName());
            System.out.println(processDefinition.getCategory());
            System.out.println(processDefinition.getVersion());
        }
    }

    @Test
    public void createHistoricTaskLogEntryQuery() {
        List<HistoricTaskLogEntry> historicTaskLogEntries = historyService.createHistoricTaskLogEntryQuery().list();
        for (HistoricTaskLogEntry historicTaskLogEntry : historicTaskLogEntries) {
            System.out.println(historicTaskLogEntry.getUserId());
            System.out.println(historicTaskLogEntry.getTaskId());
        }
    }

    @Test
    public void getResourceAsStream() {
        String deploymentId = "288bc5e6-0c05-11ea-9a48-e0d55eaf548e";
        // 获取资源名称列表，包含xml和png
        List<String> deploymentResourceNames = repositoryService.getDeploymentResourceNames(deploymentId);
        for (String deploymentResourceName : deploymentResourceNames) {
            if (deploymentResourceName.endsWith("png")) {
                InputStream resourceAsStream = repositoryService.getResourceAsStream(deploymentId, deploymentResourceName);
                File file = new File("C:\\Users\\Lynkan\\Desktop\\test1.png");
                FileUtil.writeFromStream(resourceAsStream, file);
            }
        }
    }


    @Test
    public void startProcessInstanceByKey() {
        identityService.setAuthenticatedUserId("0907001");
        String processDefinitionKey = "QualityManagementProcess";
        Map<String, Object> variables = new HashMap<>();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey);
        System.out.println(processInstance.getId());
        System.out.println(processInstance.getActivityId());
    }


    @Test
    public void createProcessInstanceQueryByProcessInstanceId() {
        String processInstanceId = "3e0de7fb-0cf9-11ea-a526-e0d55eaf548e";
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (processInstance == null) {
            System.out.println("当前流程实例已经结束");
        } else {
            System.out.println("当前流程实例正在运行");
        }
    }

    @Test
    public void createHistoricProcessInstanceQuery() {
        List<HistoricProcessInstance> historicProcessInstances = historyService.createHistoricProcessInstanceQuery().list();
        for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
            System.out.println("---------------------------");
            System.out.println(historicProcessInstance.getProcessDefinitionId());
            System.out.println(historicProcessInstance.getStartActivityId());
            System.out.println(historicProcessInstance.getEndTime());
        }
    }

    @Test
    public void createProcessInstanceQuery() {
        List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery().list();
        for (ProcessInstance processInstance : processInstances) {
            System.out.println(processInstance.getId());
            System.out.println(processInstance.getActivityId());
        }
    }


    @Test
    public void createExecutionQuery() {
        List<Execution> executions = runtimeService.createExecutionQuery().list();
        for (Execution execution : executions) {
            System.out.println(execution.getId());
            System.out.println(execution.getActivityId());
        }
    }

    @Test
    public void createTaskQuery() {
        List<Task> tasks = taskService.createTaskQuery().taskAssignee("0907001").list();
        for (Task task : tasks) {
            System.out.println(task.getId());
            System.out.println(task.getName());
            System.out.println(task.getTaskDefinitionKey());
        }
    }
}
