package io.renren;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Slf4j
public class LogTest {
    @Test
    public void test() {
        log.trace("=====TRACE=====");
        log.debug("=====DEBUG=====");
        log.info("=====INFO=====");
        log.warn("=====WARN=====");
        log.error("=====ERROR=====");
    }
}
