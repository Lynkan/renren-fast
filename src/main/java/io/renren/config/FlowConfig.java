package io.renren.config;

import io.renren.datasource.config.DynamicDataSource;
import lombok.RequiredArgsConstructor;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.context.annotation.Configuration;

/**
 * 流引擎配置
 *
 * @author Lynkan
 */
@Configuration
@RequiredArgsConstructor
public class FlowConfig implements EngineConfigurationConfigurer<SpringProcessEngineConfiguration> {
    private final DynamicDataSource dynamicDataSource;

    @Override
    public void configure(SpringProcessEngineConfiguration springProcessEngineConfiguration) {
        springProcessEngineConfiguration.setDatabaseType("mysql");
        springProcessEngineConfiguration.setDataSource(dynamicDataSource);
        springProcessEngineConfiguration.setDrawSequenceFlowNameWithNoLabelDI(true);
    }
}
